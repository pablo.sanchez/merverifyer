# merVerifyer

This script goes through a set of sequences of a protein and checks if each of them contains all 
the essential aminoacids required for its functional activity, based on the alignment with a reference sequence.  
The positions of the essential aminoacids have to be specified according to the numeration of this reference sequence.
If a position can have the aminoacid that the reference has and another different, then there is a parameter that can
be used to specify this possible variations.   

By default, the program will output: (i) a list with the IDs of proteins that passed the verification, (ii) another with 
those that did not pass it, (iii) a pickle object with a dictionary that contains further details of the verification, and, if specified, (iv) a fasta file with the verified sequences. In the process, two intermediate files are generated: one fasta with the sequences of the targets plus the reference, and one of the alignment of all those sequences.