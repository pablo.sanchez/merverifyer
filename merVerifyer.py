#!/usr/bin/python

__version__ = "0.1"
__author__ = "Marta Ferri"
__email__ = "ferri@icm.csic.es"

import os, argparse, sys, subprocess, json, pickle

# ----------------------------------------------------------------------------------------------------------------------
#                                         ARGUMENTS
# ----------------------------------------------------------------------------------------------------------------------

# Definition of the options of the program
parser = argparse.ArgumentParser(
    prog="verifier",
    description="""This program goes through a set of sequences of a protein and checks if each of them contains all 
the essential aminoacids required for its functional activity, based on the alignment with a reference sequence.  
The positions of the essential aminoacids have to be specified according to the numeration of this reference sequence.
If a position can have the aminoacid that the reference has and another different, then there is a parameter that can
be used to specify this possible variations.  
By default, the program will output: (i) a list with the IDs of proteins that passed the verification, (ii) another with 
those that did not pass it, (iii) a pickle object with a dictionary that contains further details of the verification, and, 
if specified, (iv) a fasta file with the verified sequences. In the process, two intermediate files are generated: one 
fasta with the sequences of the targets plus the reference, and one of the alignment of all those sequences.""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog="Written by Marta Ferri, 2020. ICM-CSIC")


parser.add_argument('-ref', '--reference',
                    required=True,
                    dest="reference",
                    action="store",
                    help="reference sequence in FASTA format")

parser.add_argument('-t', '--target',
                    required=True,
                    dest="target",
                    action="store",
                    help="target sequence(s) in FASTA format")

parser.add_argument('-e', '--essential',
                    required=True,
                    dest="essential",
                    action="store",
                    help="list of positions of the essential aa, based on the reference protein (e.g. '[14, 20, 48]')")

parser.add_argument('-me', '--multiessential',
                    dest="multi_essential",
                    action="store",
                    default=None,
                    help="If a position can have another aa different than the one in the reference use this parameter"
                         "to specify the position and the accepted aa with this syntax: '14:A|C;48:Y|T'")

parser.add_argument('-o', '--output',
                    required=True,
                    dest="output",
                    action="store",
                    help="path to the desired output folder")

parser.add_argument('-n', '--name',
                    required=True,
                    dest="name",
                    action="store",
                    help="name of the run, used to name the results folder")

parser.add_argument('-TPF', '--TPfasta',
                    dest="tp_fasta",
                    action="store_true",
                    default=False,
                    help="whether you want to create a fasta file containing the true positive (TP) sequences or not. The default is to not create it.")


parser.add_argument('-v', '--version',
                    action='version',
                    version='%(prog)s 0.1')


options = parser.parse_args()


# Before starting, check Python version
assert sys.version_info >= (3, 7), "This program needs Python 3.7 or higher to run."

# ----------------------------------------------------------------------------------------------------------------------
#                                         FUNCTIONS
# ----------------------------------------------------------------------------------------------------------------------

def alignment2dict(alignment_file):
    """
    Creates a dictionary with the protein IDs as keys and the sequences as values.
    :param alignment_file: alignment file in fasta format.
    :return: the dictionary of 'header: sequence'
    """
    dic = {}
    with open(alignment_file, "r") as file:
        for line in file:
            line = line.strip()
            if line.startswith(">"):
                header = line[1:]
                dic[header] = ""
            else:
                dic[header] += line
    return dic


def verifier(alignment_file, reference_name, conserved_aa_dic):
    """
    Takes an alignment in FASTA format of protein sequences and a reference
    protein with conserved aminoacids, then it checks for these conserved aminoacids
    in the rest of the sequences.
    :param alignment_file: alignment file in fasta format.
    :param reference: name (header) of the reference protein.
    :param conserved_aa_dic: dictionary of the conserved aminoacids ('position: aa').
    :return: a dictionary with the comparisons, a list of the true sequences, and a list of the FP.
    """

    # Converting the alignment to a dictionary
    dic = alignment2dict(alignment_file)

    # Constructing an equivalence dict mapping the reference positions to the alignment positions
    count = 0
    equivalence_dic = {}  # real_pos: alignament_pos
    for index, symbol in enumerate(dic[reference_name]):
        if symbol != "-":
            equivalence_dic[count] = index
            count += 1


    # Checking if the proteins have the conserved aminoacids
    match_list = []
    unmatch_list = []
    for protein, sequence in dic.items():
        # print(f"{protein}\n")
        for real_pos, conserved_aa in conserved_aa_dic.items():
            # print(real_pos, equivalence_dic[real_pos])
            sequence_aa = sequence[equivalence_dic[real_pos]]
            if sequence_aa in conserved_aa:
                # print(f"Conserved {conserved_aa}: MATCH\n")
                if protein != reference_name:
                    match_list.append((protein, real_pos, conserved_aa, sequence_aa))
            else:
                # print(f"Conserved {conserved_aa}: UNMATCH {sequence_aa}\n")
                unmatch_list.append((protein, real_pos, conserved_aa, sequence_aa))

    final_dict = {}
    for element in unmatch_list:
        if element[0] not in final_dict.keys():
            final_dict[element[0]] = []
            final_dict[element[0]].append((element[1], element[2], element[3]))
        else:
            final_dict[element[0]].append((element[1], element[2], element[3]))
    for element in match_list:
        if element[0] not in final_dict.keys():
            final_dict[element[0]] = []
            final_dict[element[0]].append((element[1], element[2], element[3]))
        else:
            final_dict[element[0]].append((element[1], element[2], element[3]))

    unmatch_ids = set([element[0] for element in unmatch_list])
    match_ids = set([element for element in dic.keys() if element not in unmatch_ids and element != reference_name])

    return final_dict, list(match_ids), list(unmatch_ids)


def generate_output_list(main_list, output_folder, filename):
    """
    Creates an output file with each item of a list per line.
    :param main_list: list that is going to be printed in the file
    :param output_folder: path of the folder where the file will be created
    :param filename: name of the file that will be created
    :return:
    """
    output = output_folder + "/" + filename
    with open(output, "w") as outfile:
        for element in main_list:
            outfile.write(element + "\n")

# ----------------------------------------------------------------------------------------------------------------------
#                                         INPUTS
# ----------------------------------------------------------------------------------------------------------------------
print("Parsing the input parameters.")

# Define input variables
reference = options.reference
target = options.target
essential_list = json.loads(options.essential)
pre_especial_dic = options.multi_essential if options.multi_essential else None
output_path = options.output
name = options.name
tp_fasta = options.tp_fasta

print(f"\tessential_list: {essential_list}, type: {type(essential_list)}")
if pre_especial_dic:
    print(f"\tpre_especial_dic: {pre_especial_dic}, type: {type(pre_especial_dic)}")
    especial_dic = {}
    for element in pre_especial_dic.split(";"):
        pair = element.split(":")
        especial_dic[int(pair[0])] = pair[1]
    print(f"\tespecial_dic: {especial_dic}, type: {type(especial_dic)}")
else:
    especial_dic = {}

print("\n")
# ----------------------------------------------------------------------------------------------------------------------
#                                         OUTPUT FOLDER
# ----------------------------------------------------------------------------------------------------------------------

# Prepare output folder
output_folder = output_path + "/" + name
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
else:
    print("Output folder already exists, some files may be replaced.")
    answer = input("Do you want to continue? [Y|N] ")
    if answer == "N":
        sys.exit(0)

print(f"Output will be stored in {output_folder}")
print("\n")
# ----------------------------------------------------------------------------------------------------------------------
#                                         ESSENTIAL AA DICT
# ----------------------------------------------------------------------------------------------------------------------

print("Creating the essential aminoacids dictionary.")

# Prepare essential_dic
essential_dic = {}
reference_dic = alignment2dict(reference)

assert len(reference_dic.keys()) == 1, "The reference file can contain only one sequence. Please, try again."

for header, seq in reference_dic.items():
    reference_name = header
    for pos in essential_list:
        essential_dic[pos-1] = seq[pos-1]

# Check special cases and update essential_dic
if especial_dic:
    for pos, aa in especial_dic.items():
        if pos-1 in essential_dic.keys():
            essential_dic[pos-1] = aa

print(f"\tThe dictionary is: {essential_dic}")
print("\n")
# ----------------------------------------------------------------------------------------------------------------------
#                                         MERGE FASTAS
# ----------------------------------------------------------------------------------------------------------------------

print("Merging the reference and target fastas to serve as input for mafft.")

# Appending the reference fasta and the target(s) fasta
fastas_merged = output_folder + "/" + "all.fasta"

filenames = [reference, target]  # it is important to put the reference first

with open(fastas_merged, 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            outfile.write(infile.read())
        outfile.write("\n")

print(f"\tMerged fastas file created in {fastas_merged}")
print("\n")
# ----------------------------------------------------------------------------------------------------------------------
#                                         MSA
# ----------------------------------------------------------------------------------------------------------------------

print("Running mafft to create the MSA.")
# Perform the MSA
msa_file = output_folder + "/" + "all.msa"
command = f"mafft --anysymbol --thread 8 --threadtb 5 --threadit 0 --reorder --auto {fastas_merged} > {msa_file}"
print(f"\tmafft command: {command}")
subprocess.call(command, shell=True)

print(f"\tMSA file created in {msa_file}")
assert os.path.exists(msa_file) or not os.stat(msa_file).st_size == 0, "Something went wrong with the alignment, please try again."
print("\n")
# ----------------------------------------------------------------------------------------------------------------------
#                                         VERIFICATION
# ----------------------------------------------------------------------------------------------------------------------

print("Verifying the sequences based on the alignment file...")

final_dic, match_list, unmatch_list = verifier(msa_file, reference_name, essential_dic)

generate_output_list(match_list, output_folder, "match_list.txt")
generate_output_list(unmatch_list, output_folder, "unmatch_list.txt")
final_dic_pickle = output_folder + '/unmatch_info.pickle'
pickle.dump(final_dic, open(final_dic_pickle, 'wb'))

print(f"Done!\nVerified sequences: {len(match_list)}; Failed sequences:  {len(unmatch_list)}.")
# ----------------------------------------------------------------------------------------------------------------------
#                                         CREATION OF TP FASTA
# ----------------------------------------------------------------------------------------------------------------------
if tp_fasta:
    input_dic = alignment2dict(target)
    output = output_folder + '/TP_sequences.fasta'
    with open(output, "w") as outf:
        for prot in match_list:
            outf.write(f">{prot}\n{input_dic[prot]}\n")

print(f"\nOutput files created successfully in {output_folder}")
print("\n")







